package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import pageObject.generalFunction.GeneralAction;
import pageObject.generalFunction.GeneralFetch;
import pageObject.generalFunction.SupportFunction;

/**
 * Base class for all POM classes/files.
 */
public class BasePOM {

    protected WebDriver driver; // Main selenium driver
    protected Actions action; // Grants low level Selenium's Actions class function.
    protected GeneralAction act; // Grants general pre-made Actions.
    protected GeneralFetch fetch; // Grants general pre-made data fetching process.
    protected SupportFunction sprt; // Grants general supporting functions.
    protected WebElement usedElement = null; // Temporary WebElement object to store used element

    public BasePOM(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(this.driver);
        this.act = new GeneralAction(this.driver);
        this.fetch = new GeneralFetch(this.driver);
        this.sprt = new SupportFunction(this.driver);
    }
}
