package pageObject.generalFunction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * Base class for all General Functions
 */
public class BaseGeneralFunction {

    protected WebDriver driver;
    protected Actions action;
    protected WebElement usedElement = null; // Temporary WebElement object to store used element

    public BaseGeneralFunction(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(driver);
    }
}
