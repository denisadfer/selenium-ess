package factory;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * Main class to create customized 
 * <b>Selenium Driver</b> to interact with the browser.
 */
public class DriverFactory {

    public WebDriver driver;

    // Make it thread-safe (for implementation of MultiThread execution)
    public static ThreadLocal<WebDriver> threadLocalDriver = new ThreadLocal<>();

    public WebDriver init(String browser, Boolean isHeadless) {

        /*
         * Custom setting for Headless mode (Chrome only)
         */
        if (isHeadless) {
            if (browser.equals("chrome")) {
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--no-sandbox");
                options.addArguments("--headless");
                options.addArguments("--disable-dev-shm-usage");
                options.addArguments("--disable-gpu");
                options.addArguments("--ignore-ssl-errors=yes");
                options.addArguments("--ignore-certificate-errors");
                options.addArguments("--window-size=1920,1080");
                threadLocalDriver.set(new ChromeDriver(options));
            } else {
                System.out.println("Currently, Headless mode only supported for Chrome Browser.");
            }

            /*
             * Settings for normal operation
             */
        } else {
            if (browser.equals("chrome")) {
                WebDriverManager.chromedriver().setup();
                // Modified due to not target URL isn't private
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--ignore-ssl-errors=yes");
                options.addArguments("--ignore-certificate-errors");
                threadLocalDriver.set(new ChromeDriver(options));
            } else if (browser.equals("firefox")) {
                WebDriverManager.firefoxdriver().setup();
                threadLocalDriver.set(new FirefoxDriver());
            } else if (browser.equals("edge")) {
                WebDriverManager.edgedriver().setup();
                threadLocalDriver.set(new EdgeDriver());
            } else {
                System.out.println("Please pass the correct browser value: " + browser);
            }
            getDriver().manage().window().maximize();
        }

        // WebDriver's global settings
        getDriver().manage().deleteAllCookies();
        getDriver().manage().timeouts().scriptTimeout(Duration.ofSeconds(10));
        getDriver().manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        // Return created webDriver
        return getDriver();
    }

    /**
     * Main method to get initialized WebDriver instance
     * 
     * @return Initialized WebDriver / Driver instance
     */
    public static synchronized WebDriver getDriver() {
        return threadLocalDriver.get();
    }
}
