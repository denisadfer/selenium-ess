package pageObject.generalFunction;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

/**
 * Collection of methods to support GeneralFunctions and POM(s) in general
 */
public class SupportFunction extends BaseGeneralFunction {

    int defaultTimeout = 10; // in seconds
    static Boolean isSoftAssertChecked = true;

    /*
     * NOTE : Soft Assert are implemented on single threaded automation execution.
     * Need real cases where multithread/parallel execution is implemented
     * to develop better architecture that suits this type of testing.
     */

    public SupportFunction(WebDriver driver) {
        super(driver);
    }

    /**
     * Set global implicit wait time
     * 
     * @param seconds timeout in seconds
     */
    public void setImplicitWait(int seconds) {
        this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(seconds));
    }

    /**
     * Set global implicit wait time in millis
     * 
     * @param millis timeout in miliseconds (ms)
     */
    public void setImplicitWaitMillis(int millis) {
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(millis));
    }

    /**
     * Reset global implicit wait time (Default to {@value 10} second)
     */
    public void resetImplicitWait() {
        setImplicitWait(this.defaultTimeout);
    }

    /**
     * Construct a FluentWait object to be used as wait instance
     * 
     * @param timeout      in seconds
     * @param pollingNanos inspection polling interval
     * @return Wait instance (tied to used driver instance)
     */
    public Wait<WebDriver> constructFluentWait(long timeout, long pollingNanos) {
        Wait<WebDriver> customWait = new FluentWait<WebDriver>(this.driver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofNanos(1))
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementNotInteractableException.class);
        return customWait;
    }

    /**
     * Construct a FluentWait object to be used as wait instance
     * 
     * @param timeout in seconds
     * @return Wait instance (tied to used driver instance)
     */
    public Wait<WebDriver> constructFluentWait(long timeout) {
        return constructFluentWait(timeout, 1);
    }

    /**
     * Construct a FluentWait object to be used as wait instance using default
     * settings
     * 
     * @return Wait instance (tied to used driver instance)
     */
    public Wait<WebDriver> constructFluentWait() {
        return constructFluentWait(1, 1);
    }

    /**
     * Get an element instance using FluentWait with specified timeout and ignoring
     * NoSuchElementException and ElementNotInteractableException exceptions
     * 
     * @param locator
     * @param timeout
     * @return
     */
    public WebElement getElementFluently(By locator, int timeout) {
        Wait<WebDriver> customWait = constructFluentWait(timeout);
        /*
         * If failed see :
         * https://www.selenium.dev/selenium/docs/api/java/org/openqa/selenium/support/
         * ui/FluentWait.html
         */
        WebElement element = customWait.until(ExpectedConditions.elementToBeClickable(locator));
        return element;
    }

    /**
     * Get an element instance using FluentWait
     * 
     * @param locator
     * @return
     */
    public WebElement getElementFluently(By locator) {
        return getElementFluently(locator, this.defaultTimeout);
    }

    public void setSoftAssertCheckedStatus(Boolean isClear) {
        isSoftAssertChecked = isClear;
    }

    public Boolean getSoftAssertCheckedStatus() {
        return isSoftAssertChecked;
    }

    public String getPageTitle() {
        return driver.getTitle();
    }
}
