package pageObject.generalFunction;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;

/**
 * Collection of methods to fetch/obtain data from the UI
 */
public class GeneralFetch extends BaseGeneralFunction {

    SupportFunction sprt;
    Wait<WebDriver> localCustomWait; // Global variable to store local fluentWait instance

    public GeneralFetch(WebDriver driver) {
        super(driver);
        sprt = new SupportFunction(driver);
    }

    /**
     * Check whether an element exist or not
     * 
     * @param locator locator
     * @return target element existence status
     */
    public Boolean isElementExist(By locator) {
        List<WebElement> elements = driver.findElements(locator);
        return (elements.size() > 0) ? true : false;
    }

    public Boolean isElementDisplayed(By locator) {
        return driver.findElement(locator).isDisplayed();
    }

    public String getText(By locator) {
        localCustomWait = sprt.constructFluentWait();
        usedElement = localCustomWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return usedElement.getText();
    }

    public Boolean isButtonEnabled(By locator){
        return driver.findElement(locator).isEnabled();
    }

}
