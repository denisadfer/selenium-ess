# Test Automation ESS-Review

SQA Test Automation on Selenium for ESS-Review project

# PREREQUISITES
- VSCode Installed
- JDK Installed and Set the JAVA_HOME
- VSCode Extension
    - Java Extension Pack
    - Cucumber (Gherkin) Full Support

# SELENIUM - JAVA - CUCUMBER INSTALLATION ON VSCODE
1. Create Java Project > maven > archetype-quickstart-jdk8
2. Add Maven Dependencies
    - [Cucumber Java](https://mvnrepository.com/artifact/io.cucumber/cucumber-java)
    - [Cucumber jUnit](https://mvnrepository.com/artifact/io.cucumber/cucumber-junit)
    - [jUnit](https://mvnrepository.com/artifact/junit/junit )
    - [Selenium Java](https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java)
    - [GeckoDriver](https://github.com/mozilla/geckodriver/releases)
3. Modify _settings.json_ (read _Cucumber (Gherkin) Full Support_ details)
    ```
    {
    "java.dependency.packagePresentation": "hierarchical",
    "cucumberautocomplete.steps": [
      "src/test/java/StepDefinition/*/*.java",
      "src/test/java/StepDefinition/*.java"
    ],
    "cucumberautocomplete.syncfeatures": "src/test/resources/Features/*feature",
    "cucumberautocomplete.strictGherkinCompletion": true,
    "java.configuration.updateBuildConfiguration": "interactive",
    "java.jdt.ls.vmargs": "-XX:+UseParallelGC -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90 -Dsun.zip.disableMemoryMapping=true -Xmx4G -Xms100m -Xlog:disable",
    "java.compile.nullAnalysis.mode": "automatic" 
    }

    ```
# PROJECT STRUCTURE (POM PATTERN)
### Feature file
- Scenario on a feature file group by test suite on TestLink. Use testcase name as scenario name (exact) and testsuite name as feature name

### Step Definition 
- Maps all test case steps on feature file to code
- Driver initiation placed on Infrastructure/Driver/Setup.java inside of Step Definition


### Page Object
- One page object class represent one page on software under test
- Page object class include : 
    - Web element locator
    - All action on this particular page

### Test Runner

# RUN TEST AS JUNIT
## Run all testcases
1. Open TestRunner.java on \src\test\java\TestRunner\TestRunner.java
2. Remove tags on tags
    ```
	tags= "",
    ```
3. Click Run Test on TestRunner.java


## Run single testcase
1. Open TestRunner.java on \src\test\java\TestRunner\TestRunner.java
2. Put the tag above the selected test case
    ```
    Feature: Login feature
        @Test
        Scenario: TC-2:Login will not success with invalid credentials
    ```
3. Put the tag on tags
    ```
	tags= "@Test",
    ```
4. Click Run Test on TestRunner.java

# SETUP GUIDE
- Complete guide to setup the Automation Framework can be seen [here](https://docs.google.com/document/d/1DDPhblmnTEhgjd-LsWyY83RA-ONiqOsg82tXtFaSL2M/edit?usp=sharing).